@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            
            @can('users_manage')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="title">@lang('global.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.permissions.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.permissions.title')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.roles.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.roles.title')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-user"></i>
                            <span class="title">
                                @lang('global.users.title')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Menu de categories -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span class="title">@lang('global.categories.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.categories.index') }}">
                            <i class="fa fa-list-alt"></i>
                            <span class="title">
                                @lang('global.categories.menu.list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.categories.create') }}">
                            <i class="fa fa-list-alt"></i>
                            <span class="title">
                                @lang('global.categories.menu.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>

           <!-- Menu de brands -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-trademark"></i>
                    <span class="title">@lang('global.brands.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.brands.index') }}">
                            <i class="fa fa-trademark"></i>
                            <span class="title">
                                @lang('global.brands.menu.list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.brands.create') }}">
                            <i class="fa fa-trademark"></i>
                            <span class="title">
                                @lang('global.brands.menu.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>

           <!-- Menu de table -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span class="title">@lang('global.tables.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.tables.index') }}">
                            <i class="fa fa-table"></i>
                            <span class="title">
                                @lang('global.tables.menu.list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.tables.create') }}">
                            <i class="fa fa-table"></i>
                            <span class="title">
                                @lang('global.tables.menu.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li> 

          <!-- Menu de product -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span class="title">@lang('global.products.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.products.index') }}">
                            <i class="fa fa-product-hunt"></i>
                            <span class="title">
                                @lang('global.products.menu.list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.products.create') }}">
                            <i class="fa fa-product-hunt"></i>
                            <span class="title">
                                @lang('global.products.menu.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>   
                      <!-- Menu de orders -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span class="title">@lang('global.orders.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.orders.index') }}">
                            <i class="fa fa-list-alt"></i>
                            <span class="title">
                                @lang('global.orders.menu.list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.orders.report') }}">
                            <i class="fa fa-list-alt"></i>
                            <span class="title">
                                Report
                            </span>
                        </a>
                    </li>                    

                </ul>
            </li>   
                


            @endcan

                      <!-- Menu de takeorders -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span class="title">@lang('global.takeorders.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                        <a href="{{ route('admin.takeorders.take') }}">
                            <i class="fa fa-list"></i>
                            <span class="title">@lang('global.takeorders.menu.create')</span>
                        </a>
                    </li>                    

                    <li class="{{ $request->segment(2) == 'change_password' ? 'active' : '' }}">
                        <a href="{{ route('admin.takeorders.index') }}">
                            <i class="fa fa-list"></i>
                            <span class="title">
                                @lang('global.takeorders.menu.list')
                            </span>
                        </a>
                    </li>

                </ul>
            </li>   




            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">Change password</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}

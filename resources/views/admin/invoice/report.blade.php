<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Reporte</title>
    <link href="{{ url('css/pdf.css') }}" rel="stylesheet">

  </head>
  <body>

    <main>
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>Reporte Bebidas</h1>
          <div class="date">Date: {{ $date }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc">DESCRIPTION</th>
            <th class="unit">UNIT PRICE</th>
          </tr>
        </thead>
        <tbody>  
        @foreach($drinks as $drink)  
          <tr>
            <td class="desc">{{ $drink->name }}</td>
            <td class="unit">{{ $drink->price }}</td>
          </tr>
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>{{$total_d}}</td>
          </tr>
        </tfoot>
      </table>
      


      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>Reporte Comidas</h1>
          <div class="date">Date: {{ $date }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc">DESCRIPTION</th>
            <th class="unit">UNIT PRICE</th>
          </tr>
        </thead>
        <tbody>  
        @foreach($foods as $food)  
          <tr>
            <td class="desc">{{ $food->name }}</td>
            <td class="unit">{{ $food->price }}</td>
          </tr>
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>{{$total_f}}</td>
          </tr>
        </tfoot>
      </table>



  </body>
</html>
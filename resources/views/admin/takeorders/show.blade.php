@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.orders.title')</h3>
    <p> 
        <a href="{{ url('admin/takeorders') }}" class="btn btn-success">@lang('global.app_back')</a>
    </p>    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('id', 'Order Number', ['class' => 'control-label']) !!}
                    {!! Form::text('id', $order->id, ['class' => 'form-control']) !!}
                </div>
            </div>
          <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('order_status', 'Order Status', ['class' => 'control-label']) !!}
                    @switch($order->order_status)
                        @case(1)
                            {!! Form::text('order_status', 'Open', ['class' => 'form-control']) !!}
                            @break
                        @case(2)
                            {!! Form::text('order_status', 'Cancel', ['class' => 'form-control']) !!}
                            @break  
                        @default
                            {!! Form::text('order_status', 'Close', ['class' => 'form-control']) !!}
                    @endswitch                                                      

                </div>
            </div>
          <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('user_id', 'User', ['class' => 'control-label']) !!}
                    {!! Form::text('user_id', $order->user->name, ['class' => 'form-control']) !!}
                </div>
            </div>   
          <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('table_id', 'Table', ['class' => 'control-label']) !!}
                    {!! Form::text('table_id', $order->table->name, ['class' => 'form-control']) !!}
                </div>
            </div> 
          <div class="row">
                <div class="col-xs-12 form-group">

                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Product</th>
                          <th scope="col">Price</th>
                          <th scope="col">Observation</th>
                        </tr>
                      </thead>
                      <tbody>
                    @if (count($items) > 0)

                        @foreach ($items as $item)

                                <tr>
                                  <td>{{$item->product->name}}</td>
                                  <td>{{$item->price}}</td>
                                  <td>{{$item->observation}}</td>
                                </tr>
                        @endforeach  
                    @endif          
                      </tbody>
                    </table>







                </div>
            </div> 


        </div>
    </div>



@stop


@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.takeorders.title')</h3>
    <p>
        <a href="{{ route('admin.takeorders.take') }}" class="btn btn-success">@lang('global.app_add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($takeorders) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>

                        <th>@lang('global.takeorders.fields.id')</th>
                        <th>@lang('global.takeorders.fields.creation_date')</th>
                        <th>@lang('global.takeorders.fields.order_status')</th>
                        <th>@lang('global.takeorders.fields.table')</th>

                        <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($takeorders) > 0)
                        @foreach ($takeorders as $order)
                            <tr data-entry-id="{{ $order->id }}">
                                <td></td>

                                <td>{{ $order->id }}</td>
                                <td>{{ $order->creation_date }}</td>
                                <td>
                                    @switch($order->order_status)
                                         @case(1)
                                            <span class="label label-primary">Open</span>
                                            @break

                                        @case(2)
                                            <span class="label label-danger">Cancel</span>
                                            @break

                                        @default
                                            <span class="label label-success">Close</span>
                                    @endswitch

                                </td>
                                <td>{{ $order->table->name }}</td>
                                <td>
                                    @if($order->order_status < 2)
                                        
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PUT',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.takeorders.update', $order->id])) !!}
                                        {!! Form::submit(trans('global.app_close'), array('class' => 'btn btn-xs btn-success')) !!}
                                        {!! Form::close() !!}
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.takeorders.destroy', $order->id])) !!}
                                        {!! Form::submit(trans('global.app_cancel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                        @if(1==2)
                                <a href="{{ route('admin.takeorders.edit', $order->id) }}" class="btn btn-xs btn-primary">edit</a>
                                @endif
                                    @endif
                                <a href="{{ route('admin.takeorders.show', $order->id) }}" class="btn btn-xs btn-primary">@lang('global.app_view')</a>
                                <a href="{{ route('admin.pdf.order', $order->id) }}" target="_blank" class="btn btn-xs btn-primary">Print</a>

                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 


@endsection
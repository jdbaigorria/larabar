@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.products.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

<div class="panel-body table-responsive">

<p>
{!! Form::label('table_id', 'Table', ['class' => 'control-label']) !!}
<select id="tables_bar" class="form-control">
@foreach($tables as $table)

<option value="{{$table->id}}">{{$table->name}}</option>

@endforeach

</select>
</p>

<p>
{!! Form::label('table_filter', 'Category', ['class' => 'control-label']) !!}
<select id="table-filter" class="form-control">
<option value="">All</option>
@foreach($categories as $category)

<option>{{$category->name}}</option>

@endforeach

</select>
</p>


<table id="products" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Name</th>
            <th>Description</th>
            <th>Category</th>
            <th>Price</th>
            <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($products as $product)

        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->description}}</td>
            <td>{{$product->category->name}}</td>
            <td>{{$product->price}}</td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
</table>
    </div>


<div class="panel-body table-responsive">
{!! Form::model($order, ['method' => 'PUT', 'route' => ['admin.takeorders.takeedit', $order->id]]) !!}
<input type="hidden" name="table_id" value="" id="changevalue" />

<table class="table" id="order">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Product</th>
      <th scope="col">Price</th>
      <th scope="col">Observation</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
@foreach($items as $key => $item)   
<tr>
    <th><input type="hidden" name="product_id[{{$key}}]" value="{{$productItem[$key]->id}}">{{$productItem[$key]->id}}</th>
    <td>{{$productItem[$key]->name}}</td>
    <td><input type="hidden" name="price[{{$key}}]" value="{{$item->price}}">{{$item->price}}</td>
    <td><input type="text"  name="observation[{{$key}}]">{{$item->observation}}</td>
    <td><input type="button"  value="-" class="del btn-danger btn-xs"/></td>
</tr>
@endforeach
  </tbody>
</table>



{!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}


</div>    

</div>







@stop

@section('javascript') 

    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.products.mass_destroy') }}';
    </script>

<script>

$(document).ready(function (){
    var table = $('#products').DataTable({
        dom: 'lrtip',
        "bPaginate": false,
        "bLengthChange": false,
    });
    
    $('#table-filter').on('change', function(){
       table.search(this.value).draw();   
    });
});


$('#products tr').on('click', function(){
    var codigo = $(this).find('td:first').html();
    var product = $(this).find('td:nth-child(2)').html();
    var price = $(this).find('td:nth-child(5)').html();
    var row = '<tr><th><input type="hidden" name="product_id[]" value="'+codigo+'">'+codigo+'</th><td>'+product+'</td><td><input type="hidden" name="price[]" value="'+price+'">'+price+'</td><td><input type="text"  name="observation[]"></td><td><input type="button"  value="-" class="del btn-danger btn-xs"/></td></tr>';

    $("#order").append(row);

});

$("#order").on("click", ".del", function(){

    $(this).parents("tr").remove();

});

var initial = $("#tables_bar").find("option:selected").val();
$("#changevalue").val(initial);


$("#tables_bar").change(function () {
    var selected = $(this).find("option:selected").val();

   $("#changevalue").val(selected);

});

  
</script>


@endsection
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.orders.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">

<p>
{!! Form::label('table_filter', 'Status', ['class' => 'control-label']) !!}
<select id="table-filter" class="form-control">
<option value="">All</option>
<option value="1">Open</option>
<option value="3">Close</option>
</select>
</p>





<table id="products" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td style="display:none;"></td>
                        <th>#</th>
                        <th>@lang('global.orders.fields.date')</th>
                        <th>@lang('global.orders.fields.status')</th>
                        <th>@lang('global.orders.fields.employee')</th>
                        <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($orders) > 0)
                        @foreach ($orders as $order)
                            <tr data-entry-id="{{ $order->id }}">
                                <td style="display:none;">{{$order->order_status}}</td>
                                <td>{{$order->id}}</td>
                                <td>{{ $order->creation_date }}</td>
                                <td>
                                    @switch($order->order_status)
                                         @case(1)
                                            <span class="label label-primary">Open</span>
                                            @break

                                        @case(2)
                                            <span class="label label-danger">Cancel</span>
                                            @break

                                        @default
                                            <span class="label label-success">Close</span>
                                    @endswitch
                                    
                                </td>
                                <td>{{ \Auth::user()->name }}</td>
                                <td>
                                <a href="{{ route('admin.orders.show', $order->id) }}" class="btn btn-xs btn-primary">@lang('global.app_view')</a>

                                    @if($order->order_status < 2)
                                    <a href="{{ route('admin.orderitems.show',[$order->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>                                    
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PUT',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.orders.update', $order->id])) !!}
                                        {!! Form::submit(trans('global.app_close'), array('class' => 'btn btn-xs btn-success')) !!}
                                        {!! Form::close() !!}

                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.orders.destroy', $order->id])) !!}
                                        {!! Form::submit(trans('global.app_cancel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}

                                    @endif 
                                <a href="{{ route('admin.pdf.order', $order->id) }}" target="_blank" class="btn btn-xs btn-primary">Print</a>                                                                       
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 

<script type="text/javascript">
 $(document).ready(function (){
    var table = $('#products').DataTable({
        dom: 'lrtip',
        "bPaginate": false,
        "bLengthChange": false,
    });
    
    $('#table-filter').on('change', function(){
       table.column().search(this.value).draw();   
    });


});
   

</script>

@endsection
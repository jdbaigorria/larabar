<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Order #{{$order->id}}</title>
    <link href="{{ url('css/pdf.css') }}" rel="stylesheet">

  </head>
  <body>

    <main>
   @if($drinks)   
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>ORDER {{ $order->id }}</h1>
          <h3>Drinks</h3>
          <div class="date">Date: {{ $order->creation_date }}</div>
          <div class="date">Table: {{ $table->name }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc">DESCRIPTION</th>
            <th class="desc">CATEGORY</th>
            <th class="unit">UNIT PRICE</th>
            <th class="total">OBSERVATION</th>
          </tr>
        </thead>
        <tbody>  
        @foreach($items as $key => $item)  
        @if($products[$key]->product_type == 1)
          <tr>
            <td class="desc">{{ $products[$key]->name }}</td>
            <td class="desc">{{ $categories[$products[$key]->category_id]}}</td>
            <td class="unit">{{ $item->price }}</td>
            <td class="total">{{ $item->observation }} </td>
          </tr>
         @endif
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>{{$total_f}}</td>
          </tr>
        </tfoot>
      </table>
      @endif

@if($foods)
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>ORDER {{ $order->id }}</h1>
          <h3>Foods</h3>
          <div class="date">Date: {{ $order->creation_date }}</div>
          <div class="date">Table: {{ $table->name }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc">DESCRIPTION</th>
            <th class="desc">CATEGORY</th>
            <th class="unit">UNIT PRICE</th>
            <th class="total">OBSERVATION</th>
          </tr>
        </thead>
        <tbody>  
        @foreach($items as $key => $item)  
        @if($products[$key]->product_type == 2)
          <tr>
            <td class="desc">{{ $products[$key]->name }}</td>
            <td class="desc">{{ $categories[$products[$key]->category_id]}}</td>
            <td class="unit">{{ $item->price }}</td>
            <td class="total">{{ $item->observation }} </td>
          </tr>
         @endif
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>{{$total_d}}</td>
          </tr>
        </tfoot>
      </table>
@endif

      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>ORDER {{ $order->id }}</h1>
          <div class="date">Date: {{ $order->creation_date }}</div>
          <div class="date">Table: {{ $table->name }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc">DESCRIPTION</th>
            <th class="desc">CATEGORY</th>
            <th class="unit">UNIT PRICE</th>
            <th class="total">OBSERVATION</th>
          </tr>
        </thead>
        <tbody>  
        @foreach($items as $key => $item)  
          <tr>
            <td class="desc">{{ $products[$key]->name }}</td>
            <td class="desc">{{ $categories[$products[$key]->category_id]}}</td>
            <td class="unit">{{ $item->price }}</td>
            <td class="total">{{ $item->observation }} </td>
          </tr>
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>{{$total}}</td>
          </tr>
        </tfoot>
      </table>

  </body>
</html>
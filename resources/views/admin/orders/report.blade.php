@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.orders.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">


    {!! Form::open(['method' => 'GET', 'route' => ['admin.orders.generate']]) !!}

    <div class="panel panel-default">
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('min', 'Start Date', ['class' => 'control-label']) !!}
                    {!! Form::text('min', old('min'), ['class' => 'form-control', 'id' => 'min']) !!}       
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('max', 'End Date', ['class' => 'control-label']) !!}
                    {!! Form::text('max', old('max'), ['class' => 'form-control', 'id' => 'max']) !!}
                </div>
            </div>            
        </div>
    </div>

    {!! Form::submit('Generate', ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}







        </div>
    </div>
@stop

@section('javascript') 



@endsection
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.orderitems.title')</h3>

    <p>
        <a href="{{ route('admin.orders.index') }}" class="btn btn-success">@lang('global.app_back')</a>
    </p>
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($orderItems) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>

                        <th>@lang('global.orderitems.fields.product')</th>
                        <th>@lang('global.orderitems.fields.observation')</th>
                        <th>@lang('global.orderitems.fields.price')</th>
                        <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($orderItems) > 0)
                        @foreach ($orderItems as $item)
                            <tr data-entry-id="{{ $item->id }}">
                                <td></td>

                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->observation }}</td>
                                <td>{{ $item->price }}</td>
                                <td>
                                    <a href="{{ route('admin.orderitems.edit',[$item->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.orderitems.destroy', $item->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 



@endsection
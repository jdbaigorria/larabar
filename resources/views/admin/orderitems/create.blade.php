@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.orderitems.title')</h3>
    {!! Form::model($orderItem, ['method' => 'PUT', 'route' => ['admin.orderitems.update', $orderItem->id]]) !!}

    {{ Form::hidden('order_id', $orderItem->order_id) }}
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('product_id', 'Product', ['class' => 'control-label']) !!}
                    {!! Form::select('product_id', $products, $orderItem->product_id, ["class" => "form-control"])!!}
                </div>
            </div>  
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('observation', 'Observation', ['class' => 'control-label']) !!}
                    {!! Form::text('observation', old('observation'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('observation'))
                        <p class="help-block">
                            {{ $errors->first('observation') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
                    {!! Form::text('price', $orderItem->price, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('price'))
                        <p class="help-block">
                            {{ $errors->first('price') }}
                        </p>
                    @endif
                </div>
            </div>






        </div>
    </div>
    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
     <a href="{{ route('admin.orderitems.show',[$orderItem->order_id]) }}" class="btn btn-info">@lang('global.app_back')</a>                                    

    {!! Form::close() !!}

@stop


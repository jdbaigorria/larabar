<?php
Route::get('/', function () { return redirect('/admin/home'); });

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('categories', 'CategoryController');
    Route::post('category._mass_destroy', ['uses' => 'CategoryController@massDestroy', 'as' => 'categories.mass_destroy']);
    Route::resource('brands', 'BrandController');
    Route::post('brand._mass_destroy', ['uses' => 'BrandController@massDestroy', 'as' => 'brands.mass_destroy']); 
    Route::resource('jobs', 'JobController');
    Route::post('job._mass_destroy', ['uses' => 'JobController@massDestroy', 'as' => 'jobs.mass_destroy']);  
    Route::resource('employees', 'EmployeeController');
    Route::post('employee._mass_destroy', ['uses' => 'EmployeeController@massDestroy', 'as' => 'employees.mass_destroy']);
    Route::resource('products', 'ProductController');
    Route::post('product._mass_destroy', ['uses' => 'ProductController@massDestroy', 'as' => 'products.mass_destroy']);
    Route::resource('takeorders', 'TakeOrderController');
    Route::get('take', ['uses' => 'TakeOrderController@take', 'as' => 'takeorders.take']);
    Route::put('take/{id}', ['uses' => 'TakeOrderController@takeUpdate', 'as' => 'takeorders.takeedit']);

    Route::resource('orders', 'OrderController');
    Route::post('order._mass_destroy', ['uses' => 'OrderController@massDestroy', 'as' => 'orders.mass_destroy']);    
    Route::resource('orderitems', 'OrderItemController');
    Route::post('orderitem._mass_destroy', ['uses' => 'OrderItemController@massDestroy', 'as' => 'orderitems.mass_destroy']);
    Route::resource('tables', 'TableController');
    Route::post('table._mass_destroy', ['uses' => 'TableController@massDestroy', 'as' => 'tables.mass_destroy']);

    Route::get('pdf/{id}', ['uses' => 'PdfController@invoice', 'as' => 'pdf.order']);

    Route::get('report/date', ['uses' => 'OrderController@report', 'as' => 'orders.report']);
    Route::get('report/date/generate', ['uses' => 'OrderController@generate', 'as' => 'orders.generate']);

              

});


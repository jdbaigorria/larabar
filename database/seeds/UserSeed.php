<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'username' => 'administrador',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);
        $user->assignRole('administrator');

        $vendor = User::create([
            'name' => 'Vendedor',
            'username' => 'vendedor',
            'email' => 'vendedor@vendedor.com',
            'password' => bcrypt('vendedor')
        ]);
        $user->assignRole('users');

    }
}

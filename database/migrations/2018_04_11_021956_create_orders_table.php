<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->date('creation_date');
            $table->integer('order_status');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('table_id');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users'); 
            $table->foreign('table_id')->references('id')->on('tables');            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
        
    protected $table = 'orders';

    protected $fillable =    ['creation_date', 'order_status', 'user_id', 'table_id'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    } 

    public function orderitem()
    {
    	return $this->hasOne('App\OrderItem');
    } 

    public function table()
    {
        return $this->belongsTo('App\Table');
    }     


}

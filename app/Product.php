<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';


    protected $fillable = [
            'name',
            'description',
            'comments',
            'price',
            'product_type',
            'brand_id',
            'category_id'
        ];

    public function brand()
    {
    	return $this->hasOne('App\Brand');
    }  

    public function category()
    {
        return $this->belongsTo('App\Category');

    }    

    public function orderitem()
    {
        return $this->hasOne('App\OrderItem');
    }           
}

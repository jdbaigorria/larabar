<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
   
	protected $table = 'order_items';

	protected $fillable =  ['product_id', 'order_id', 'price', 'observation'];



    public function product()
    {
    	return $this->belongsTo('App\Product');
    } 

    public function order()
    {
    	return $this->hasOne('App\Order');
    } 
}

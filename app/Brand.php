<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $table = 'brands';

    protected $fillable = [
    	'name',
    	'description', 
    	'short_description', 
    	'date_create', 
    	'date_dismiss'
    ];


    public function product()
    {
        return $this->belongsTo('App\Product');
    }    
}

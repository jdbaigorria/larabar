<?php

namespace App\Http\Controllers;

use App\OrderItem;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function show($order)
    {
        $orderItems = OrderItem::where('order_id', $order)->get();
        return view('admin.orderitems.index', compact('orderItems'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function edit($item)
    {
        $orderItem = OrderItem::where('id', $item)->first();
        $products = Product::pluck('name', 'id');
        return view('admin.orderitems.edit', compact('orderItem', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $orderItem = OrderItem::where('id', $id)->first();
          
          $fillable = $request->all();

          $orderItem->fill($fillable)->save();

          return redirect('/admin/orderitems/'.$orderItem->order_id); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $orderItem = OrderItem::where('id', $id)->first();
        
          $order_id = $orderItem->order_id;

          $orderItem->delete();

          return redirect('/admin/orderitems/'.$order_id);
    }

    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = OrderItem::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }         
}

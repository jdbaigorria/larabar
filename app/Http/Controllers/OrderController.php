<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('id', $id)->first();

        $items = OrderItem::where('order_id', $id)->get();
        return view('admin.orders.show', compact('order', 'items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $order = Order::where('id', $id);

          $order->update(['order_status' => 3]);

          return redirect('/admin/orders');        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $order = Order::where('id', $id);

          $order->update(['order_status' => 2]);

          return redirect('/admin/orders');
    }

    public function massDestroy(Request $request)
    {

    }  

    public function report()
    {
        $orders = Order::all();
        return view('admin.orders.report', compact('orders'));
    } 

    public function generate(Request $request)
    {
       // $orders = Order::whereBetween('creation_date', $request->toArray())->get();
        $date = $request['min'].' '.$request['max'];

        /*DB::table('orders')
                ->whereBetween('creation_date', $request->toArray())
                ->get();        
*/
        //return view('admin.orders.report', compact('orders'));
       

        $foods = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->where('product_type', 2)
            ->whereBetween('order_items.created_at', $request->toArray())
            ->select('order_items.order_id','products.name', 'order_items.price')->get();

        $total_f = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->where('product_type', 2)
            ->whereBetween('order_items.created_at', $request->toArray())
            ->sum('order_items.price');            

        $drinks = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->where('product_type', 1)
            ->whereBetween('order_items.created_at', $request->toArray())
            ->select('order_items.order_id','products.name', 'order_items.price')->get();

        $total_d = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->where('product_type', 1)
            ->whereBetween('order_items.created_at', $request->toArray())
            ->sum('order_items.price');



        $view =  \View::make('admin.invoice.report', compact('foods','drinks', 'total_d', 'total_f', 'date'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('report');
    }     


}

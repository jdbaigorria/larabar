<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Table;
use App\Category;
use DB;

class PdfController extends Controller
{
    public function invoice($id) 
    {
    	$order = Order::where('id',$id)->first();
    	$items = OrderItem::where('order_id', $id)->get();
    	$table = Table::where('id', $order->table_id)->first();
        $categories = Category::pluck('name', 'id');

    	$products;
        $drinks = false;
        $foods = false;

    	foreach($items as $key => $item){
    		$products[$key] = Product::where('id',$item->product_id)->first();

            if($products[$key]->product_type == 1){
                $drinks = true;
            }else{
                $foods = true;
            }


    	}

        $total_f = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->where('product_type', 2)
            ->where('order_id', $id)
            ->sum('order_items.price');

        $total_d = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->where('product_type', 1)
            ->where('order_id', $id)
            ->sum('order_items.price');



    	$total = DB::table('order_items')
                ->where('order_id', $id)
                ->sum('price');               

        $date = date('Y-m-d');
        $view =  \View::make('admin.invoice.invoice', compact('order', 'items', 'table', 'products', 'total', 'categories', 'drinks', 'foods', 'total_f', 'total_d'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');
    }


}

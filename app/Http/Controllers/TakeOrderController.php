<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Category;
use App\Employee;
use App\Order;
use App\OrderItem;
use App\Table;
use Carbon\Carbon;

class TakeOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $takeorders = Order::where('user_id', \Auth::user()->id)->get();
        return view('admin.takeorders.index', compact('takeorders'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $order = new Order;

        $fillable = ['creation_date' => Carbon::now(), 
            'order_status' => '1', 
            'user_id' => \Auth::user()->id, 
            'table_id' => $request['table_id'],
        ];

        $order->fill($fillable);

        $order->save();



        $items = $request->all();
        unset($items['_token']);



		for ($i=0; $i < count($items['product_id']); $i++) { 
			$orderitem = new OrderItem;
			$fillable = ['product_id' => $items['product_id'][$i], 
						'order_id' => $order->id, 
						'price' => $items['price'][$i], 
						'observation' => $items['observation'][$i]
					];

			$orderitem->fill($fillable);
			$orderitem->save();
		}


		$products = Product::all();
        $categories = Category::all();
        $tables = Table::all();

        return view('admin.takeorders.take', compact('products', 'categories', 'tables'));



    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('id', $id)->first();

        $items = OrderItem::where('order_id', $id)->get();
        return view('admin.takeorders.show', compact('order', 'items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::where('id', $id)->first();
        $products = Product::all();
        $categories = Category::all();
        $tables = Table::where('id', $order->table_id)->get();
        $items = OrderItem::where('order_id', $order->id)->get();
        $productItem;

        foreach($items as $key => $item)
        {
            $productItem[$key] = Product::where('id',$item->product_id)->first();
        }



        return view('admin.takeorders.edit', compact('products', 'categories', 'tables', 'items', 'productItem', 'order'));  

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
          $order = Order::where('id', $id);

          $order->update(['order_status' => 3]);

          return redirect('/admin/takeorders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $order = Order::where('id', $id);

          $order->update(['order_status' => 2]);

          return redirect('/admin/takeorders');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {

    }   

    public function take()
    {
        $products = Product::all();
        $categories = Category::all();
        $tables = Table::all();

        return view('admin.takeorders.take', compact('products', 'categories', 'tables'));        
    }  


    public function takeUpdate(Request $request, $id)
    {

        $order = Order::where('id', $id)->get();
        $orderitems = OrderItem::where('order_id', $id)->get();
        $items = $request->all();
        unset($items['_token']);

        if(sizeof($orderitems) > 0){
            foreach($orderitems as $key => $item){
                $item->delete();
            }
        }

        for ($i=0; $i < count($items['product_id']); $i++) { 
            $orderitem = new OrderItem;
            $fillable = ['product_id' => $items['product_id'][$i], 
                        'order_id' => $id, 
                        'price' => $items['price'][$i], 
                        'observation' => $items['observation'][$i]
                    ];

            $orderitem->fill($fillable);
            $orderitem->save();
        }


        $takeorders = Order::where('user_id', \Auth::user()->id)->get();
        return view('admin.takeorders.index', compact('takeorders'));      


    }


}
